# Схема данных

## Задача:

1. Разработать структуру хранения квот и цен для возможности бронирования номерного фонда отеля
2. Квоты и цены должны храниться с горизонтом 1 год назад и 2 года вперед;
3. Представить результат в виде ER-диаграммы, с учетом специфики 1С-Битрикс;
4. Продумать отказоустойчивую и масштабируемую серверную архитектуру для хранения данных. 
5. Представить результат по серверной архитектуре в виде диаграммы с описательной частью, почему выбрано именно это решение 


## Cхема данных:

![scheme](./data_scheme.png)


## Организационная схема микросервиса:

![scheme](./scheme/org.png)

## Схема развертывания приложения:

![scheme](./scheme/deploy.png)

